//
//  ThreadPool.h
//  Samples
//
//  Created by Harshit Gupta on 3/15/14.
//  Copyright (c) 2014 Harshit Gupta. All rights reserved.
//

#ifndef Samples_ThreadPool_h
#define Samples_ThreadPool_h

typedef int threadId;

#include "Stack.h"
#include <pthread.h>
#include <stdbool.h>

/*!
 *  Data structure representing the variables used to manage of the thread pool.
 */
typedef struct
{
	pthread_t *threads;
	Stack stack;
	int used;
	int max;
} ThreadPool;

/*!
 *  Initializes the threadpool with the number of thread asked.
 *
 *  @param maxSize number of threads
 */
ThreadPool* initThreadPool(int maxSize);

/*!
 *  Deallocates the memory assigned to the thread pool.
 *
 *  @param threadPool   pointer refrence to the threadPool
 */
void destroyThreadPool(ThreadPool *threadPool);

/*!
 *  Checks if the threadPool is empty
 *
 *  @param threadPool   pointer refrence to the threadPool
 *
 *  @return true if empty, false otherwise.
 */
bool isThreadPoolEmpty(ThreadPool *threadPool);

/*!
 *  Checks if the threadPool is full
 *
 *  @param threadPool   pointer refrence to the threadPool
 *
 *  @return true if full, false otherwise.
 */
bool isThreadPoolFull(ThreadPool *threadPool);


/*!
 *  addes the thread specified to the threadPool to be reused.
 *
 *  @param threadPool   pointer refrence to the threadPool
 *  @param element the threadId to be pushed
 */
void addThreadToPool(ThreadPool *threadPool, threadId element);

/*!
 *  Takes one available thread from the threadpool and returns it
 *
 *  @param threadPool   pointer refrence to the threadPool
 *
 *  @return  element threadId from the threadpool
 */
threadId getThreadFromPool(ThreadPool *threadPool);

#endif
