//
//  PartitionerOutputManager.h
//  MapReduce
//
//  Created by Harshit Gupta on 4/24/14.
//  Copyright (c) 2014 Harshit Gupta. All rights reserved.
//

#ifndef MapReduce_PartitionerOutputManager_h
#define MapReduce_PartitionerOutputManager_h

#include "MapOutputManager.h"

/*!
 *  This function makes the partitions of the key-list pairs from combiner output manager into the number of files asked based upon the hash function.
 *
 *  @param combinerOutputManager pointer reference to combiner output manager
 *  @param numberOfPartitions    number of files tobe created
 *  @param outputFilenameFormat  format for the output file name
 */
void partitionData(MapOutputMgr *outputManager,
				   int numberOfPartitions,
				   char *outputFilenameFormat);

#endif
