
#ifndef FOO_H_   /* Include guard */
#define FOO_H_

#include <stdbool.h>

/*!
 *  This function return the size of a give file path.
 *
 *  @param pathName the file path
 *
 *  @return file size if the path is a regular file, and readable for everyone -1, otherwise
 */
long accessibleFileSize(const char *pathName);

/*!
 *  checks if the file is accessible.
 *
 *  @param pathName the file path
 *
 *  @return true if accessible, false otherwise
 */
bool isFileAccessible(char *filename);

#endif