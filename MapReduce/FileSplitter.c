//
//  FileSplitter.c
//  Samples
//
//  Created by Harshit Gupta on 4/23/14.
//  Copyright (c) 2014 Harshit Gupta. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/uio.h>
#include <unistd.h>
#include <fcntl.h>

#include "FileSplitter.h"
#include "NetworkConstants.h"
#include "utility.h"

typedef struct
{
	char inputFileName[MaxLengthFileName];
	char outputFileFormat[MaxLengthFileName];
	int threadID;
	unsigned long long offset;
	unsigned long long estimatedBytesInFile;
} ThreadData;

/*!
 *  Split files on the basis of the thread data passed to it.
 *
 *  @param threadData containig all the details the thread require to work
 *
 *  @return NULL
 */
void *split(void *threadData);


bool splitFile(char *inputFileName, char *outputFileFormat, int totalParts)
{
	pthread_t thread[totalParts];
	ThreadData **threadData = (ThreadData **) malloc (totalParts * sizeof(ThreadData *));
	
	unsigned long long fileSize = accessibleFileSize(inputFileName);
	unsigned long long estimatedBytesInFile = fileSize/totalParts;
	int i = 0;
	for (i = 0; i < totalParts; i++) {
		threadData[i] = (ThreadData *) malloc(sizeof(ThreadData));
		strncpy(threadData[i]->inputFileName, inputFileName, strlen(inputFileName));
		strncpy(threadData[i]->outputFileFormat, outputFileFormat, strlen(outputFileFormat));
		threadData[i]->threadID = i;
		threadData[i]->offset = estimatedBytesInFile * i;
		threadData[i]->estimatedBytesInFile = estimatedBytesInFile;
		
		pthread_create(&thread[i], NULL, split, threadData[i]);
	}
	
	for (i = 0; i < totalParts; i++)
		pthread_join(thread[i], NULL);
	
	free(threadData);
	return true;
}

void *split(void *threadData)
{
	ThreadData data = *(ThreadData *)threadData;
	
	char ch[2];
	ssize_t len = 1;
	size_t lenRead = 0;
	int fileReader, fileWriter;
	
	fileReader = open(data.inputFileName, O_RDONLY);
	if (fileReader < 1)
		exit(EXIT_FAILURE);
	
	if (data.offset != 0)
	{
		pread(fileReader, ch, len, data.offset - 1);
		while (ch[0] != '\n')
		{
			if (ch[0] == EOF)
				return NULL;
			pread(fileReader, ch, len, data.offset++);
			data.estimatedBytesInFile--;
		}
	}
	
	char fileoutputname[MaxLengthFileName];
	sprintf(fileoutputname, data.outputFileFormat, data.threadID);
	fileWriter = creat(fileoutputname, S_IRUSR | S_IWUSR |S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH);
	
	unsigned long long bytesWritten = 0;
	unsigned long long tempOffset = data.offset;
	while ((lenRead = pread(fileReader, ch, len, tempOffset++)) > 0)
	{
		if (lenRead<1) {
			exit(EXIT_FAILURE);
		}
		pwrite(fileWriter, ch, lenRead, bytesWritten);
		bytesWritten += lenRead;
		if (ch[0] == '\n') {
			if (bytesWritten >= data.estimatedBytesInFile)
				break;
		}
	}
	free(threadData);
	close(fileReader);
	close(fileWriter);
	return NULL;
}
