//
//  BPlusTree.h
//  MapReduce
//
//  Created by Harshit Gupta on 9/13/14.
//  Copyright (c) 2014 Harshit Gupta. All rights reserved.
//

#ifndef MapReduce_BPlusTree_h
#define MapReduce_BPlusTree_h

#include "BPTConstants.h"

typedef struct
{
	bool isLeaf;
	int noOfKeys;
	
	uint32_t keys[treeOrder];
	int keyIndeces[treeOrder];
	void *pointers[treeOrder + 1];
} BPTNode;

BPTNode* initBPTNode();

int searchKeyInTree(BPTNode *node, uint32_t hash);
BPTNode* insertKey(BPTNode *root, uint32_t key, int index);

void deleteTree(BPTNode *root);

#endif
