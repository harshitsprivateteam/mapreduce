//
//  NetworkConstants.h
//  Samples
//
//  Created by Harshit Gupta on 3/25/14.
//  Copyright (c) 2014 Harshit Gupta. All rights reserved.
//

#ifndef Samples_NetworkConstants_h
#define Samples_NetworkConstants_h

#include <sys/socket.h>		//for socket(), connect(), ...
#include <netinet/in.h>		//for internet address family
#include <arpa/inet.h>		//for sockaddr_in, inet_addr()

#define ERROR -1

#define SocketLength sizeof(Socket)
#define MPMessageSize sizeof(MPMessage)

#define MaxLengthConfigLine 80
#define MaxLengthFileName 25
#define MaxLengthIPAddr 15

#define MaxMessageSize  200
#define MaxKeySize 1024
#define MaxValueSize 1024

#define MaxNoOfClients 128
#define MaxFilesSend 8

typedef struct sockaddr_in Socket;
typedef int SocketDescriptor;

/*!
 *  The enum struture represents all the possible values of a signal that can be sent from a node to other.
 */
typedef enum
{
	STCodeReady,
	STReduceInputReady,
	STResultReady,
	STFileNeeded,
	STHeartbeat
} SignalType;

/*!
 *  The data structure holds the data which can be exchanged among nodes.
 */
typedef struct
{
	unsigned long long startOffset, endOffset;
}Offset;

typedef struct
{
	int numberOfFiles;
	char names[MaxFilesSend][MaxLengthFileName];
	char filename[MaxLengthFileName];
	SignalType signalType;
	Offset offset;
	int port;
} MPMessage;

#endif
