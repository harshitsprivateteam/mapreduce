//
//  Stack.c
//  Samples
//
//  Created by Harshit Gupta on 3/15/14.
//  Copyright (c) 2014 Harshit Gupta. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include "Stack.h"

void initStack(Stack *stack, int maxSize)
{
	stackElementT *newContents;
	
	newContents = (stackElementT *) malloc(maxSize * sizeof(stackElementT));
	
	stack->contents = newContents;
	stack->maxSize = maxSize;
	stack->top = -1;
}

void destroyStack(Stack *stack)
{
	free(stack->contents);
	
	stack->contents = NULL;
	stack->maxSize = 0;
	stack->top = -1;
}

int isStackEmpty(Stack *stack)
{
	return stack->top < 0;
}

int isStackFull(Stack *stack)
{
	return stack->top >= (stack->maxSize - 1);
}

void stackPush(Stack *stack, stackElementT element)
{
	if (isStackFull(stack))
	{
		fprintf(stderr, "Can't push element on stack: stack is full.\n");
		exit(1);
	}
	else
		stack->contents[++stack->top] = element;
}

stackElementT stackPop(Stack *stack)
{
	if (isStackEmpty(stack))
	{
		fprintf(stderr, "Can't pop element from stack: stack is empty.\n");
		exit(1);
	}
	else
		return stack->contents[stack->top--];
}


