//
//  FDMapReduce.c
//  MapReduce
//
//  Created by Harshit Gupta on 4/24/14.
//  Copyright (c) 2014 Harshit Gupta. All rights reserved.
//

#include <stdio.h>			//for printf()
#include <sys/types.h>		//for data types
#include <stdlib.h>			//for atoi() and exit()
#include <string.h>			//for memset()
#include <unistd.h>			//for close()
#include <errno.h>			//for system error numbers
#include <fcntl.h>			//for open()
#include <sys/stat.h>       //for chmod()

#include "utility.h"
#include "NetworkConstants.h"

#include "NodesManager.h"
#include "ThreadPool.h"
#include "FileSplitter.h"

#include "PartitionerOutputManager.h"
#include "MapReduce.h"

#define ProjectName "FDMapReduce"

#ifdef __unix__
#define ConfigFile "config.txt"
#elif defined __APPLE__
#define ConfigFile "config_m.txt"
#endif


#define Result "result"
#define PartitionerInputFileFormat "part_%d_%s.asc"
#define ReduceInputFileFormat "part_%d_%d.asc"
#define ReduceInputFileFormatS "part_%s_%d.asc"
#define ReduceOutputFileFormat "result_%d.asc"

#define KeyFileName "key"

typedef struct
{
	long double cpuUtiz;
	long double freeMem;
	unsigned numOfCores;
} SystemInfo;

SystemInfo emptyData = {0};

#define TotalColumns 4
#define ColumnWidth 18
#define TotalWidth ColumnWidth*TotalColumns + TotalColumns + 3

SystemInfo loadInfo;

pthread_mutex_t printMutex= PTHREAD_MUTEX_INITIALIZER;
char **printString;

/*!
 *
 */
void displayArgI(char *msg, int arg);

/*!
 *
 */
void displayArg(char *msg, char *arg);

/*!
 *  Display the message on console and flushs the stream inorder to make its shown.
 *
 *  @param msg the message to be displayed.
 */
void display(char *msg);

/*!
 *  Whenever some error occurs, this methods is called. It displays the error message and then exits the whole programs.
 *
 *  @param message the error message to be displayed.
 */
void exitUponError(char *message);

/*!
 *  This methods is called when the program has done doing what it is supposed to do. It also deallocates any of the variables allocated in the program globally.
 */
void destroyVariablesAndExit();

/*!
 *  The variable helps the program keep its index in track in order to interact with other nodes.
 */
int selfIndex;

/*!
 *  The variable keeps the track of the name of the data-input file. And pass it to others nodes where needed.
 */
char inputFileName[MaxLengthFileName];

Offset offset;

int reduceInputFileCount = 0;

bool taskDone;
/*!
 *  This getter method returns localized index of "selfIndex" variable, which is basically mapped to index according the number of data nodes in the cluster
 *
 *  @param index selfIndex
 *
 *  @return an interger which is the localised index.
 */
int localisedIndex(int index);

/*!
 *  This getter method return the caller, a refrence to the information associated with the local node.
 *
 *  @return pointer refrence of type Node
 */
Node* localNode();

/*!
 *  Description
 *
 *  @return return value description
 */
Node* nameNode();

/*!
 *  This returns reference to node which has the same ip address and listens to the same port number as mentioned.
 *
 *  @param ipAddr character array having the ipAddr to be matched.
 *  @param port   int port number to be matched.
 *
 *  @return pointer refrence of type Node
 */
Node* nodeWithIPNPort(char *ipAddr, int port);

/*!
 *  This getter function returns to refrence to the local node manager, which is mostly used to find a refrence to other nodes.
 *
 *  @return pointer refrence of type Node Manager
 */
NodesManager* localNodeManager();

/*!
 *  This getter function returns to refrence to the thread pool. A thread manages everything relaetd to thread like how many are currently used, how many available. It works on the basis of reusability.
 *
 *  @return pointer refrence of type thread pool
 */
ThreadPool* localThreadPool();

/*!
 *
 */
void *heartbeat(void *argv);

/*!
 *  This pointer functions is function which is associated to the listening thread.
 *
 *  @param threadData a data struture containing the information that might be needed by the thread.
 *
 *  @return NULL
 */
void *startListening(void *threadData);

/*!
 *  When the listening thread recieves a new request, it passes to it this pointer function and itself goes back to the listening cycle.
 *
 *  @param threadData a data struture containing the information that might be needed by the thread.
 *
 *  @return NULL
 */
void *serveClient(void *threadData);

/*!
 *  This method is to send a message to the node specified in the arguments. It creates socket and then sends the data.
 *
 *  @param node    Node, data to be sent to.
 *  @param message the data structure, MPMessage which needs to be sent.
 */
void sendSignalToNode(Node *node, MPMessage message);

/*!
 *
 */
void *getNodeStatus(void *argv);

/*!
 *
 */
void *monitorDataNodes(void *data);

/*!
 *
 */
void *refreshSystemLoad(void *data);

/*!
 *
 */
void measureSystemLoad();
/*!
 *  When the listening thread receives message, indicating it to download some files, then it assignes to task
 *
 *  @param message the data structure of the type MPMessage containing the information about the files that need tobe downloaded.
 *  @param node    the node where where it needs to be downloaded.
 */
void downloadFiles(MPMessage message, Node *node);

/*!
 *  This pointer functions is further called by downloadFiles method to download teh files in parallel.
 *
 *  @param threadData a data struture containing the information that might be needed by the thread.
 *
 *  @return NULL
 */
void *downloadFile(void *threadData);

/*!
 *  This function checks if the file is executable, from the name of of the file.
 *
 *  @param filename filename to check for
 *
 *  @return bool value, true if executable, false otherwise.
 */
bool isFileExecutable(char *filename);

/*!
 *  Returns the index of the first data node.
 *
 *  @return integer value
 */
int indexOfFirstDataNode();

/*!
 *  Returns the number of total data nodes in the cluster.
 *
 *  @return integer value.
 */
int numberOfDataNodes();

/*!
 *  This function is called by the program if it is running at client end. It tells the program running on the name node, to download the executables and other files if available.
 */
void initializeNameNode(void);

/*!
 *  This function is called by the program if it is running at name node. It tells the program running on the data nodes, to download the executables and other files if available.
 */
void initializeDataNodes(void);

/*!
 *  This function is called by the program if it is running at data node. It first calls the map function, then combiner , followed by partitioner. At the last, it informs the other data nodes that input files for the reducing functionality is available.
 */
void runCoreFunctionality(void);

/*!
 *  This function starts called reducing function of every list, Once all the requires files are downloaded.
 */
void startReducing(void);

/*!
 *  This function is called at the data node, when result files from the reduce functions are available, to inform the name node that about the outcome.
 *
 *  @param outputFilename name of the output file.
 */
void sendResultToNameNode(char* outputFilename);

/*!
 *  This function is called at the name node, when it has received all the result files from the all the data nodes, to inform the client to download them.
 */
void sendResultToClientNode(char* outputFilename);

/*!
 *  This functions is called to determine if the required files are available to proceed further functioning.
 *
 *  @param filenameFormat format of the files to check for.
 *
 *  @return true if all files available, false even if one is missing.
 */
bool areFilesWithFormatAvailableToProceed(char *filenameFormat);

/*!
 *  This function is called to verify if files that were supposed to be downloaded by other nodes, have been sent or not. The application of the function comes in to power once when a machine wants to exit.
 *
 *  @param filenameFormat format of the files to check for.
 *
 *  @return true if all files sent, false even if one is left.
 */
bool allFilesSentWithFormat(char *filenameFormat);

/*!
 *  This function determine the type of the node the program is running upon and later runs the actual functionality it is supoosed to run based upon the earlier decision.
 */
void setup(void);

/*!
 *  This data structure holds the data that needs to be transfer to the function pointer.
 */
typedef struct
{
	SocketDescriptor remoteSocketDes;
	threadId threadNumber;
	Node *node;
	char filename[MaxLengthFileName];
} ThreadData;

int main(int argc, const char * argv[])
{
	argc > 1 ? selfIndex = atoi(argv[1]) : exitUponError("Insufficient Argument. Configuration Index needed.");
	
	if (selfIndex < 0 || selfIndex >= localNodeManager()->used)
		exitUponError("Invalid argument.");
	
	if (localNode()->nodeType == NTClient || localNode()->nodeType == NTNameNode)
		argc > 2 ? strcpy(inputFileName, argv[2]) : exitUponError("Insufficient Argument. Input file name needed.");
	else if(argc > 4)
	{
		strcpy(inputFileName, argv[2]);
		offset.startOffset = atoll(argv[3]);
		offset.endOffset = atoll(argv[4]);
	}
	else
		exitUponError("Insufficient Argument. Offsets needed at data node.");
	
	taskDone = false;
	
	
	pthread_t listenThread;
	if (localNode()->nodeType != NTClient )
	{
		pthread_t heartbeatThread;
		if (localNode()->nodeType == NTNameNode)
			pthread_create(&heartbeatThread, NULL, monitorDataNodes, NULL);
		else
		{
			pthread_t heartbeatReqListenThread;
			pthread_create(&heartbeatReqListenThread, NULL, heartbeat, NULL);
			pthread_create(&heartbeatThread, NULL, refreshSystemLoad, NULL);
		}
	}
	
	pthread_create(&listenThread, NULL, startListening, NULL);
	setup();
	pthread_join(listenThread, NULL);
	display("program ending");
	return 0;
}

#pragma Network Interaction

void *heartbeat(void *argv)
{
	SocketDescriptor localSocketDes, remoteSocketDes;
	struct sockaddr_in localSocket, remoteSocket;
	socklen_t socketLength = sizeof(struct sockaddr_in);
	
	if ((localSocketDes = socket(AF_INET, SOCK_STREAM, PF_UNSPEC)) == ERROR)
		exitUponError("heartbeat - socket");
	
	bzero(&localSocket, socketLength);
	localSocket.sin_family = AF_INET;
	localSocket.sin_addr.s_addr = htonl(INADDR_ANY);
	localSocket.sin_port = htons(localNode()->heartbeatPort);
	
	if (bind(localSocketDes, (struct sockaddr *) &localSocket, socketLength) == ERROR)
		exitUponError("heartbeat - bind");
	
	if ((listen(localSocketDes, SOMAXCONN)) == ERROR)
		exitUponError("heartbeat - listen");
	
	while (true)
	{
		if ((remoteSocketDes = accept(localSocketDes, (struct sockaddr *) &remoteSocket, &socketLength)) == ERROR)
			exitUponError("heartbeat - accept");
		if (send(remoteSocketDes, &loadInfo, sizeof(loadInfo), 0) < 1)
			exitUponError("heartbeat - send");
		close(remoteSocketDes);
	}
	close(localSocketDes);
	return NULL;
}

void *startListening(void *data)
{
	pthread_t thread;
	ThreadData *threadData = NULL;
	
	SocketDescriptor localSocketDes, remoteSocketDes;
	Socket localSocket, remoteSocket;
	socklen_t socketLength;
	
	if ((localSocketDes = socket(AF_INET, SOCK_STREAM, 0)) == ERROR)
		exitUponError("socket");
	
	memset(&localSocket, 0, sizeof(localSocket));
	localSocket.sin_family = AF_INET;
	localSocket.sin_addr.s_addr = htonl(INADDR_ANY);
	localSocket.sin_port = htons(localNode()->port);
	
	if (bind(localSocketDes, (struct sockaddr *) &localSocket, sizeof(localSocket)) == ERROR)
		exitUponError("bind");
	
	if (listen(localSocketDes, MaxNoOfClients) == ERROR)
		exitUponError("listen");
	
	socketLength = sizeof(Socket);
	ThreadPool *threadPool = localThreadPool();
	
	while(1)
	{
		remoteSocketDes = accept(localSocketDes,
								 (struct sockaddr *) &remoteSocket,
								 &socketLength);
		
		if (remoteSocketDes == ERROR)
			exitUponError("accept");
		
		if (isThreadPoolEmpty(threadPool))
		{
			close(remoteSocketDes);
			display("Refusing connection: Reason: Reached Max Limit\n");
			continue;
		}
		
		// create new thread
		threadId id = getThreadFromPool(threadPool);
		if (id == -1)
		{
			close(remoteSocketDes);
			display("Exception Occured. Refusing connection.\n");
			continue;
		}
		
		thread = threadPool->threads[id];
		
		threadData = (ThreadData *) malloc(sizeof(ThreadData));
		threadData->remoteSocketDes = remoteSocketDes;
		threadData->threadNumber = id;
		threadData->node = NULL;
		pthread_create(&thread, NULL, serveClient, threadData);
	}
	
	close(localSocketDes);
	return NULL;
}

void *serveClient(void *data)
{
	ThreadData threadData = *(ThreadData *)data;
	MPMessage message = {0};
	
	size_t msgLength = recv(threadData.remoteSocketDes, &message, MPMessageSize, 0);
	if (message.signalType != STFileNeeded)
		close(threadData.remoteSocketDes);
	
	if (threadData.node == NULL)
		threadData.node = nodeWithIPNPort(localNode()->ipAddr, message.port);
	
	if (msgLength == ERROR || msgLength == 0)
		perror("FR - recv");
	else
	{
		int fileDescriptor = 0;
		char content[MaxMessageSize];
		switch (message.signalType)
		{
			case STReduceInputReady:
			{
				display("STReduceInputReady_Recv");
				reduceInputFileCount++;
				if (reduceInputFileCount == numberOfDataNodes())
					startReducing();
				//				char filenameFormat[MaxLengthFileName];
				//				sprintf(filenameFormat, ReduceInputFileFormatS, "%d", localisedIndex(selfIndex));
				//				if (areFilesWithFormatAvailableToProceed(filenameFormat))
				//		startReducing();
				break;
			}
			case STResultReady:
				display("STResultReady Recv");
				
				if (localNode()->nodeType == NTNameNode)
					sendResultToClientNode(message.filename);
				else if (localNode()->nodeType == NTClient)
				{
					downloadFiles(message, nameNode());
					if (areFilesWithFormatAvailableToProceed(ReduceOutputFileFormat))
					{
						display("Output Received. Run 'ls' command to see the files\n");
						destroyVariablesAndExit();
					}
				}
				break;
				
			case STFileNeeded:
				display("STFileNeeded Recv");
				fileDescriptor = open(message.filename, O_RDONLY);
				if (fileDescriptor > 0)
				{
					while((msgLength = read(fileDescriptor, content, MaxMessageSize)) > 0)
						send(threadData.remoteSocketDes, content, msgLength, 0);
					close(threadData.remoteSocketDes);
					
					//					displayArg("File named %s has been sent\n", message.filename);
					if (localNode()->nodeType == NTNameNode)
					{
						static int count = 0;
						count++;
						
						if (count == numberOfDataNodes())
							destroyVariablesAndExit();
					}
					
				}
				else
					exitUponError(message.filename);
				break;
			default:
				break;
		}
	}
	
	free(data);
	addThreadToPool(localThreadPool(), threadData.threadNumber);
	pthread_detach(pthread_self());
	return NULL;
}

char * Line(char strings[][ColumnWidth])
{
	char *line = (char *) calloc(TotalWidth, sizeof(char));
	
	int noOfSpaces = ColumnWidth;
	sprintf(line,
			"\r|%*s%*c|%*s%*c|%*s%*c|%*s%*c|",
			((noOfSpaces- (int)strlen(strings[0])) >> 1) + (int)strlen(strings[0]),
			strings[0],
			((noOfSpaces- (int)strlen(strings[0])) >> 1) + ((noOfSpaces- (int)strlen(strings[0])) & 1),
			' ',
			((noOfSpaces- (int)strlen(strings[1])) >> 1) + (int)strlen(strings[1]),
			strings[1],
			((noOfSpaces- (int)strlen(strings[1])) >> 1) + ((noOfSpaces- (int)strlen(strings[1])) & 1),
			' ',
			((noOfSpaces- (int)strlen(strings[2])) >> 1) + (int)strlen(strings[2]),
			strings[2],
			((noOfSpaces- (int)strlen(strings[2])) >> 1) + ((noOfSpaces- (int)strlen(strings[2])) & 1),
			' ',
			((noOfSpaces- (int)strlen(strings[3])) >> 1) + (int)strlen(strings[3]),
			strings[3],
			((noOfSpaces- (int)strlen(strings[3])) >> 1) + ((noOfSpaces- (int)strlen(strings[3])) & 1),
			' '
			);
	return line;
}

void print(char *message, char *positionSpecifier)
{
	if (!pthread_mutex_lock(&printMutex)) /* using lock not being sure if stdout is thread-safe or not*/
	{
		printf("%s", positionSpecifier);               /* setting the cursor position along y-axis*/
		printf("%s", message);                /* printing the string from the starting of the line*/
		fflush(stdout);
		pthread_mutex_unlock(&printMutex);
	}
}

void *monitorDataNodes(void *data)
{
	int numberOfNodes = numberOfDataNodes();
	pthread_t thread_id[numberOfNodes];
	int array[numberOfNodes];
	
	char strings[TotalColumns][ColumnWidth] = {"Data node", "CPU Load", "Free Mem", "# Cores"};
	printString = (char **)calloc((numberOfNodes + 1), sizeof(char *));
	printString[0] = (char *) calloc(TotalWidth, sizeof(char));
	strncpy(printString[0], Line(strings), TotalWidth);
	printf("%s", printString[0]);
	
	int startIndex = indexOfFirstDataNode();
	int lastIndex = startIndex + numberOfNodes;
	int i = 0;
	for(i = startIndex; i < lastIndex; i++)
	{
		array[i] = i;
		printString[i+1] = (char *) calloc(TotalWidth, sizeof(char));
		pthread_create (&thread_id[i], NULL , getNodeStatus, &array[i]);
	}
	
	for(i = startIndex; i < lastIndex; i++)
		pthread_join(thread_id[i],NULL);
	
	system("clear");
	return NULL;
}

void *getNodeStatus(void *argv)
{
	int threadNumber = *((int*)argv);
	Node *node = localNodeManager()->nodeList[threadNumber];
	
	int socketDescriptor;
	struct sockaddr_in remoteSocket;
	int dataSize = sizeof(SystemInfo);
	SystemInfo loadInfo;
	char positionSpecifier[ColumnWidth];
	char strings[TotalColumns][ColumnWidth];
	char temp[20];
	bool flag = false;
	while (1)
	{
		loadInfo = emptyData;
		
		if ((socketDescriptor = socket(AF_INET, SOCK_STREAM, 0)) != ERROR)
		{
			remoteSocket.sin_family = AF_INET;
			remoteSocket.sin_port = htons(node->heartbeatPort);
			remoteSocket.sin_addr.s_addr = inet_addr(node->ipAddr);
			bzero(&remoteSocket.sin_zero, 8);
			
			flag = true;
			if ((connect(socketDescriptor, (struct sockaddr *) &remoteSocket, sizeof(struct sockaddr_in))) != ERROR)
			{
				if(recv (socketDescriptor, &loadInfo, dataSize, 0) < 1)
				{
					//			perror("recv");
					//			pthread_exit(NULL);
					flag = false;
				}
			}
			else
			{
				//			perror("connect");
				//			pthread_exit(NULL);
				flag = false;
			}
			
		}
		else
		{
			//			perror("socket");
			//			pthread_exit(NULL);
		}
		
		close(socketDescriptor);
		memset(temp, '\0', 20);
		
		sprintf(temp, "%d", threadNumber);
		strncpy(strings[0], temp, ColumnWidth);
		
		if (!flag)
		{
			strncpy(strings[1], "---", ColumnWidth);
			strncpy(strings[2], "---", ColumnWidth);
			strncpy(strings[3], "---", ColumnWidth);
		}
		else
		{
			sprintf(temp, "%Lf", loadInfo.cpuUtiz);
			strncpy(strings[1], temp, ColumnWidth);
			
			sprintf(temp, "%Lf", loadInfo.freeMem);
			strncpy(strings[2], temp, ColumnWidth);
			
			sprintf(temp, "%d", loadInfo.numOfCores);
			strncpy(strings[3], temp, ColumnWidth);
		}
		strncpy(printString[threadNumber + 1], Line(strings), TotalWidth);
		sprintf(positionSpecifier, "\x1B[%d;0f",threadNumber);
		print(printString[threadNumber + 1], positionSpecifier);
		sleep(1);
	}
	return NULL;
}

void *refreshSystemLoad(void *data)
{
	while (true)
		measureSystemLoad();
	return NULL;
}

void measureSystemLoad()
{
#ifdef __unix__
	long double a[4], b[4];
	FILE *fp;
	char nCoreChar[4];
	memset(nCoreChar, '\0', 4);
	
	fp = fopen("/proc/stat","r");
	fscanf(fp,"%*s %Lf %Lf %Lf %Lf",&a[0],&a[1],&a[2],&a[3]);
	fclose(fp);
	sleep(1);
	
	fp = fopen("/proc/stat","r");
	fscanf(fp,"%*s %Lf %Lf %Lf %Lf",&b[0],&b[1],&b[2],&b[3]);
	fclose(fp);
	
	loadInfo.cpuUtiz = ((b[0]+b[1]+b[2]) - (a[0]+a[1]+a[2])) / ((b[0]+b[1]+b[2]+b[3]) - (a[0]+a[1]+a[2]+a[3]));
	
	fp = fopen("/proc/meminfo","r");
	fscanf(fp,"%*s %Lf kB\n %*s %Lf",&a[0], &loadInfo.freeMem);
	fclose(fp);
	
	fp = popen("grep -c \"cpu cores\" /proc/cpuinfo","r");
	fgets(nCoreChar, sizeof(nCoreChar)-1, fp);
	loadInfo.numOfCores = atoi(nCoreChar);
	fclose(fp);
#elif defined __APPLE__
	loadInfo.numOfCores = loadInfo.numOfCores == 0 ? 1 : 0;
	sleep(1);
#endif
}

void downloadFiles(MPMessage message, Node *node)
{
	ThreadPool *threadPool = localThreadPool();
	pthread_t thread;
	pthread_t usedThreadArray[message.numberOfFiles];
	int actualThreadsCount = 0;
	ThreadData *threadData = NULL;
	int i = 0;
	for (i  = 0; i < message.numberOfFiles; i++)
	{
		if (!isThreadPoolEmpty(threadPool))
		{
			threadId id = getThreadFromPool(threadPool);
			if (id != -1)
			{
				thread = threadPool->threads[id];
				
				threadData = (ThreadData *) malloc(sizeof(ThreadData));
				threadData->threadNumber = id;
				threadData->node = node;
				strcpy(threadData->filename, message.names[i]);
				pthread_create(&thread, NULL, downloadFile, threadData);
				
				usedThreadArray[actualThreadsCount] = thread;
				actualThreadsCount++;
			}
			else
				printf("Exception Occured. Closing connection.\n");
		}
		else
			printf("Reached Max Limit simultaneous downloads\n");
	}
	for (i  = 0; i < actualThreadsCount; i++)
		pthread_join(usedThreadArray[i], NULL);
}

void *downloadFile(void *data)
{
	ThreadData threadData = *(ThreadData *)data;
	
	SocketDescriptor remoteSocketDes;
	Socket remoteSocket;
	
	if ((remoteSocketDes = socket(AF_INET, SOCK_STREAM, PF_UNSPEC)) == ERROR)
		exitUponError("socket");
	
	remoteSocket.sin_family = AF_INET;
	remoteSocket.sin_port = htons(threadData.node->port);
	remoteSocket.sin_addr.s_addr = inet_addr(threadData.node->ipAddr);
	bzero(&remoteSocket.sin_zero, 8);
	
	if ((connect(remoteSocketDes, (struct sockaddr *) &remoteSocket, SocketLength)) == ERROR)
		exitUponError("connect");
	
	MPMessage message = {0};
	strcpy(message.filename, threadData.filename);
	message.signalType = STFileNeeded;
	send(remoteSocketDes, &message, MPMessageSize, 0);
	
	int fileDescriptor;
	long msgLength = 0;
	char content[MaxMessageSize];
	
	unlink(threadData.filename);
	
	fileDescriptor = open(threadData.filename, O_CREAT| O_WRONLY, S_IRUSR | S_IWUSR |S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH);
	
	if (fileDescriptor > 0)
		while ((msgLength = recv(remoteSocketDes, content, MaxMessageSize, 0)) > 0)
			write(fileDescriptor, content, msgLength);
	else
		display("\n$$file error#$\n");
	
	displayArg("File downloaded\n", threadData.filename);
	
	close(remoteSocketDes);
	close(fileDescriptor);
	free(data);
	addThreadToPool(localThreadPool(), threadData.threadNumber);
	return NULL;
}

void sendSignalToNode(Node *node, MPMessage message)
{
	SocketDescriptor remoteSocketDes;
	Socket remoteSocket;
	
	if ((remoteSocketDes = socket(AF_INET, SOCK_STREAM, 0)) == ERROR)
		exitUponError("socket");
	
	remoteSocket.sin_family = AF_INET;
	remoteSocket.sin_port = htons(message.signalType == STCodeReady ? node->ftpPort : node->port);
	remoteSocket.sin_addr.s_addr = inet_addr(node->ipAddr);
	bzero(&remoteSocket.sin_zero, 8);
	
	if ((connect(remoteSocketDes,
				 (struct sockaddr *) &remoteSocket,
				 SocketLength)) != ERROR)
		send(remoteSocketDes, &message, MPMessageSize, 0);
	else
		perror("FS - connect");
	close(remoteSocketDes);
	
	if (taskDone)
		destroyVariablesAndExit();
}

#pragma Getter Methods

int localisedIndex(int index)
{
	return index - indexOfFirstDataNode();
}

NodesManager* localNodeManager()
{
	static NodesManager *nodesManager;
	if (nodesManager == NULL)
		isFileAccessible(ConfigFile) ? nodesManager = initNodesManager(ConfigFile) : exitUponError(ConfigFile);
	return nodesManager;
}

Node* localNode()
{
	return localNodeManager()->nodeList[selfIndex];
}

Node* nameNode()
{
	static Node *nameNode;
	if (nameNode == NULL)
	{
		int i;
		for (i = 0; i < localNodeManager()->used; i++)
		{
			if (localNodeManager()->nodeList[i]->nodeType == NTNameNode)
			{
				nameNode = localNodeManager()->nodeList[i];
				break;
			}
		}
	}
	return nameNode;
}

Node* nodeWithIPNPort(char *ipAddr, int port)
{
	Node *node = NULL;
	NodesManager *nodesManager = localNodeManager();
	int i = 0;
	for (i = 0; i < nodesManager->used; i++)
		if (!strcmp(nodesManager->nodeList[i]->ipAddr, ipAddr) && nodesManager->nodeList[i]->port == port)
		{
			node = nodesManager->nodeList[i];
			break;
		}
	
	return node;
}

ThreadPool* localThreadPool()
{
	static ThreadPool *threadPool;
	if (threadPool == NULL)
		threadPool = initThreadPool(MaxNoOfClients);
	return threadPool;
}

int indexOfFirstDataNode()
{
	static int indexOfFirstDataNode = 0;
	if (indexOfFirstDataNode == 0)
	{
		int i = 0;
		for (i = 0; i < localNodeManager()->used; i++)
			if (localNodeManager()->nodeList[i]->nodeType == NTDataNode)
			{
				indexOfFirstDataNode = i;
				break;
			}
	}
	return indexOfFirstDataNode;
}

int numberOfDataNodes()
{
	static int numberOfDataNodes = 0;
	if (numberOfDataNodes == 0)
	{
		int i = 0;
		for (i = 0; i < localNodeManager()->used; i++)
			if (localNodeManager()->nodeList[i]->nodeType == NTDataNode)
				numberOfDataNodes++;
	}
	return numberOfDataNodes;
}

#pragma Exit Methods

void exitUponError(char *message)
{
	perror(message);
	exit(0);
}

void destroyVariablesAndExit()
{
	if (localNode()->nodeType == NTNameNode)
			system("clear");
	//
	//	if (printString)
	//	{
	//		int i;
	//		for(i = 0; i <= numberOfDataNodes(); i++)
	//			free(printString[i]);
	//
	//		free(printString);
	//	}
	//
	//	pthread_mutex_destroy(&printMutex);
	exit(0);
}

#pragma Console Manager

void displayArgI(char *msg, int arg)
{
	//	char temp[MaxLengthFileName];
	//	sprintf(temp, msg, arg);
	display(msg);
}

void displayArg(char *msg, char *arg)
{
	//	char temp[MaxLengthFileName];
	//	sprintf(temp, msg, arg);
	display(msg);
}

void display(char *msg)
{
	if (localNode()->nodeType != NTNameNode) {
		fprintf(stdout, "%s\n" , msg);
		fflush(stdout);
	}
}

#pragma Core Methods

void setup(void)
{
	switch (localNode()->nodeType)
	{
		case NTClient:
			display("Client");
			initializeNameNode();
			break;
			
		case NTNameNode:
			display("NameNode");
			if (!isFileAccessible(inputFileName))
				exitUponError(inputFileName);
			initializeDataNodes();
			//removing the splitting overhead
			//			else if(splitFile(inputFileName, MapInputFileFormat, numberOfDataNodes()))
			//				initializeDataNodes();
			//			else
			//				exitUponError("Error In Splitting");
			break;
			
		case NTDataNode:
			display("DataNode");
			runCoreFunctionality();
			break;
			
		default:
			exitUponError("main: invalid value");
			break;
	}
}

void runCoreFunctionality(void)
{
	MapOutputMgr mapOutputMgr;
	initMapOutputManager(&mapOutputMgr);
	isFileAccessible(inputFileName) ? map(KeyFileName,
										  inputFileName,
										  &mapOutputMgr,
										  offset.startOffset,
										  offset.endOffset) : exitUponError(inputFileName);
	char filename[MaxLengthFileName];
	sprintf(filename, PartitionerInputFileFormat, localisedIndex(selfIndex), "%d");
	partitionData(&mapOutputMgr, numberOfDataNodes(), filename);
	//	freeMapOutputManager(&mapOutputMgr);
	//	system("clear && printf '\e[3J'");
	
	int i = 0;
	for (i = 0; i < localNodeManager()->used; i++)
	{
		if (localNodeManager()->nodeList[i]->nodeType == NTDataNode)
		{
			if (i == selfIndex)
				continue;
			
			MPMessage message = {0};
			message.signalType = STReduceInputReady;
			message.numberOfFiles = 0;
			message.port = localNode()->port;
			displayArgI("sending STReduceInputReady to neighbor", i);
			sendSignalToNode(localNodeManager()->nodeList[i], message);
		}
	}
	
	reduceInputFileCount++;
	if (reduceInputFileCount == numberOfDataNodes())
		startReducing();
	//	char filenameFormat[MaxLengthFileName];
	//	sprintf(filenameFormat, ReduceInputFileFormatS, "%d", localisedIndex(selfIndex));
	//	if (areFilesWithFormatAvailableToProceed(filenameFormat))
	//		startReducing();
}

void startReducing(void)
{
	display("startReducing");
	MapOutputMgr outputMgr;
	char inputFilenameFormat[MaxLengthFileName];
	sprintf(inputFilenameFormat, ReduceInputFileFormatS, "%d", localisedIndex(selfIndex));
	initMapOutputManagerWithFile(&outputMgr, numberOfDataNodes(), inputFilenameFormat);
	
	char outputFilename[MaxLengthFileName];
	sprintf(outputFilename, ReduceOutputFileFormat, localisedIndex(selfIndex));
	int outputFileDescriptor = creat(outputFilename, S_IRUSR | S_IWUSR |S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH);
	char *output;
	size_t currentOffset = 0;
	size_t bytesWritten = 0;
	output = malloc(MAXOUTPUTLENGTH * sizeof(char));
	int i = 0;
	for (i = 0; i < outputMgr.used; i++)
	{
		memset(output, '\0', MAXOUTPUTLENGTH);
		reduce(outputMgr.pairs[i]->key,
			   outputMgr.pairs[i]->list.list,
			   outputMgr.pairs[i]->list.used,
			   output);
		bytesWritten = pwrite(outputFileDescriptor, output, strlen(output), currentOffset);
		currentOffset += bytesWritten;
		//		displayArg("%s", output);
	}
	if (output != NULL)
	{
		free(output);
		output = NULL;
	}
	close(outputFileDescriptor);
	//	freeMapOutputManager(&outputMgr);
	sendResultToNameNode(outputFilename);
}

void sendResultToClientNode(char* outputFilename)
{
	MPMessage message = {0};
	message.signalType = STResultReady;
	message.numberOfFiles = 1;
	message.port = localNode()->port;
	strcpy(message.names[0], outputFilename);
	
	int i = 0;
	for (i = 0; i < localNodeManager()->used; i++)
		if (localNodeManager()->nodeList[i]->nodeType == NTClient)
		{
			sendSignalToNode(localNodeManager()->nodeList[i], message);
			break;
		}
}

void sendResultToNameNode(char* outputFilename)
{
	int i;
	for (i = 0; i < localNodeManager()->used; i++)
	{
		if (localNodeManager()->nodeList[i]->nodeType == NTNameNode)
		{
			MPMessage message = {0};
			message.signalType = STResultReady;
			message.numberOfFiles = 0;
			message.port = localNode()->port;
			strcpy(message.filename, outputFilename);
			taskDone = true;
			sendSignalToNode(localNodeManager()->nodeList[i], message);
			break;
		}
	}
}

void initializeDataNodes(void)
{
	int totalParts = numberOfDataNodes();
	Offset offset[totalParts];
	
	int fileReader = open(inputFileName, O_RDONLY);
	
	unsigned long long fileSize = accessibleFileSize(inputFileName);
	unsigned long long estimatedBytesInFile = fileSize/totalParts;
	unsigned long long startOffset = 0, endOffset = 0;
	
	char ch[2];
	size_t lenRead = 0;
	ssize_t len = 1;
	
	int i = 0;
	
	for (i = 0; i < totalParts; i++)
	{
		endOffset = estimatedBytesInFile * (i + 1);
		endOffset--;
		
		while ((lenRead = pread(fileReader, ch, len, endOffset++)) > 0)
		{
			if (lenRead<1)
				exit(EXIT_FAILURE);
			
			if (ch[0] == '\n')
				break;
			else if (ch[0] == EOF)
				endOffset --;
		}
		
		offset[i].startOffset = startOffset;
		offset[i].endOffset = endOffset;
		startOffset = endOffset + 1;
	}
	
	int j;
	i = 0;
	for (j = 0; j < localNodeManager()->used; j++)
	{
		if (localNodeManager()->nodeList[j]->nodeType == NTDataNode)
		{
			MPMessage message = {0};
			message.signalType = STCodeReady;
			message.numberOfFiles = 0;
			message.port = localNode()->port;
			strcpy(message.filename, inputFileName);
			message.offset = offset[i++];
			
			sendSignalToNode(localNodeManager()->nodeList[j], message);
		}
	}
}

void initializeNameNode(void)
{
	int i = 0, j = 0;
	for (i = 0; i < localNodeManager()->used; i++)
	{
		if (localNodeManager()->nodeList[i]->nodeType == NTNameNode)
		{
			if(!isFileAccessible(ProjectName))
				exitUponError(ProjectName);
			char *filenames[] = {ProjectName};
			MPMessage message = {0};
			message.signalType = STCodeReady;
			message.numberOfFiles = 1;
			message.port = localNode()->port;
			strcpy(message.filename, inputFileName);
			for (j = 0; j < message.numberOfFiles && j < MaxFilesSend; j++)
				strcpy(message.names[j], filenames[j]);
			
			sendSignalToNode(localNodeManager()->nodeList[i], message);
			break;
		}
	}
}

#pragma Helper Methods

bool isFileExecutable(char *filename)
{
	bool flag = true;
	int i = 0;
	for (i = 0; i < strlen(filename); i++)
		if (filename[i] == '.')
		{
			flag = false;
			break;
		}
	
	return flag;
}

bool areFilesWithFormatAvailableToProceed(char *filenameFormat)
{
	bool flag = true;
	char filename[MaxLengthFileName];
	int numberOfFilesNeeded = numberOfDataNodes();
	
	int i = 0;
	for (i = 0; i < numberOfFilesNeeded && i < MaxFilesSend; i++)
	{
		memset(filename, '\0', MaxLengthFileName);
		sprintf(filename, filenameFormat, i);
		if (!isFileAccessible(filename))
		{
			flag = false;
			break;
		}
	}
	return flag;
}

bool allFilesSentWithFormat(char *filenameFormat)
{
	bool flag = true;
	char filename[MaxLengthFileName];
	int numberOfFilesNeeded = numberOfDataNodes();
	
	int i = 0;
	for (i = 0; i < numberOfFilesNeeded && i < MaxFilesSend; i++)
	{
		memset(filename, '\0', MaxLengthFileName);
		sprintf(filename, filenameFormat, i);
		if (isFileAccessible(filename))
		{
			flag = false;
			break;
		}
	}
	return flag;
}
