//
//  MapOutputManager.c
//  MapReduce
//
//  Created by Harshit Gupta on 4/22/14.
//  Copyright (c) 2014 Harshit Gupta. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>              // for calloc()
#include <string.h>              // for strncpy(), strlen()
#include <unistd.h>
#include <fcntl.h>
#include <stdbool.h>

#include "murmur3.h"
#include "BPlusTree.h"
#include "MapOutputManager.h"

#define MaxLengthFileName 25
#define MaxKeySize 1024
#define MaxValueSize 1024

BPTNode *root = NULL;

void extractFile(MapOutputMgr *outputManager, char *fileDescriptor);

#pragma Map Output Manager Functions

void initMapOutputManager(MapOutputMgr *outputManager)
{
	int initialSize = 1;
	outputManager->pairs = (Pair **)calloc(initialSize, sizeof(Pair *));
	outputManager->used = 0;
	outputManager->size = initialSize;

	root = initBPTNode();
}

void initMapOutputManagerWithFile(MapOutputMgr *outputManager,
								  int numberOfFiles,
								  char  *filenameFormat)
{
	initMapOutputManager(outputManager);
	
	char filename[MaxLengthFileName];
	int i = 0;
	FILE *fileDescriptor = NULL;
	for (i = 0; i < numberOfFiles; i++)
	{
		sprintf(filename, filenameFormat, i);
		fileDescriptor = fopen(filename, "r");
		if (fileDescriptor != NULL) {
//			printf("file to be extracted :%s\n\n", filename);
			extractFile(outputManager, filename);
			fclose(fileDescriptor);
		}
		else
			perror(filename);
	}
}

void extractFile(MapOutputMgr *outputManager, char *filename)
{
	int  fileReader = open(filename, O_RDONLY);
	
	char ch[2];
	char tempKey[MaxKeySize], tempValue[MaxKeySize];
	memset(ch, '\0', sizeof(ch));
	memset(tempKey, '\0', sizeof(tempKey));
	memset(tempValue, '\0', sizeof(tempValue));
	
	bool isKeyLoaded = false;
	
	int keyCount = 0;
	int valueCount = 0;
	size_t offset = 0, bytesRead = 0;
		
	while ((bytesRead = pread(fileReader, ch, 1, offset)) > 0)
	{
		offset += bytesRead;
		if (ch[0] == '\n')
		{
			isKeyLoaded = false;
			keyCount = 0;
			memset(tempKey, '\0', sizeof(tempKey));
		}
		else if (ch[0] == Splitter)
		{
			if(!isKeyLoaded)
				isKeyLoaded = true;
			else
			{
				//printf("key:------%s\n", tempKey);
				updateWithPair(outputManager, tempKey, tempValue);
				valueCount = 0;
				memset(tempValue, '\0', sizeof(tempValue));
			}
		}
		else if (ch[0] != '\0')
			isKeyLoaded ? (tempValue[valueCount++] = ch[0]) : (tempKey[keyCount++] = ch[0]);
	}
}

int indexForKeyInPairs(MapOutputMgr *outputManager, char *key)
{
	int index = 0;
	char tempKey[MaxValueSize];
	Pair *pair;
	for (index = 0; index < outputManager->used; index++)
	{
		pair = outputManager->pairs[index];
		sprintf(tempKey, "%s", pair->key);
		if (strcmp(tempKey, key) == 0)
			break;
	}
	return index < outputManager->used ? index : -1;
}


void updateWithPair(MapOutputMgr *outputManager, char *key, char *value)
{
	//	uint32_t hash[1];
	//	MurmurHash3_x86_32(key, (int)strlen(key), seed, hash);
	int index;
	//	if ((index = searchKeyInTree(root, hash[0])) == -1)
	if ((index = indexForKeyInPairs(outputManager, key)) == -1)
	{
		if (outputManager->used == outputManager->size)
		{
			outputManager->size *= 2;
			outputManager->pairs = (Pair **)realloc(outputManager->pairs, outputManager->size * sizeof(Pair *));
		}
		
		outputManager->pairs[outputManager->used] = (Pair *)calloc(1, sizeof(Pair));
		outputManager->pairs[outputManager->used]->key = (char *)calloc(strlen(key) + 1, sizeof(char));
		initList(&outputManager->pairs[outputManager->used]->list);
		
		stpncpy(outputManager->pairs[outputManager->used]->key, key, strlen(key));
		addValueToList(&outputManager->pairs[outputManager->used]->list, value);
		
		//		root = insertKey(root, hash[0], (int)outputManager->used);
		
		outputManager->used++;
	}
	else
	{
		addValueToList(&outputManager->pairs[index]->list, value);
	}
}

//void updateWithPair(MapOutputMgr *outputManager, char *key, char *value)
//{
//	uint32_t hash[1];
//	MurmurHash3_x86_32(key, (int)strlen(key), 23, hash);
//
////	printf("Key: %s, Hash : %u, Value: %s\n", key, hash[0], value);
//
//	int index;
//	if ((index = searchKeyInTree(root, hash[0])) == -1)
//	{
//		if (outputManager->used == outputManager->size)
//		{
//			outputManager->size *= 2;
//			outputManager->pairs = (Pair **)realloc(outputManager->pairs, outputManager->size * sizeof(Pair *));
//		}
//		
//		outputManager->pairs[outputManager->used] = (Pair *)calloc(1, sizeof(Pair));
//		outputManager->pairs[outputManager->used]->key = (char *)calloc(strlen(key) + 1, sizeof(char));
//		initList(&outputManager->pairs[outputManager->used]->list);
//		
//		stpncpy(outputManager->pairs[outputManager->used]->key, key, strlen(key));
//		addValueToList(&outputManager->pairs[outputManager->used]->list, value);
//		
////		printf("Before:  adding new key to tree\n");
//		root = insertKey(root, hash[0], (int)outputManager->used);
////		printf("After: adding new key to tree\n");
//
//		outputManager->used++;
//	}
//	else
//	{
////		printf("Before: adding new value to list\n");
//		addValueToList(&outputManager->pairs[index]->list, value);
////		printf("After: adding new value to list\n");
//	}
//}

void freeMapOutputManager(MapOutputMgr *outputManager)
{
	int i = 0;
	
	for (i = 0; i < outputManager->used ; i++)
	{
		freeList(&outputManager->pairs[i]->list);
		free(outputManager->pairs[i]->key);
		free(outputManager->pairs[i]);
	}
	
	free(outputManager->pairs);
	outputManager->pairs = NULL;
	outputManager->used = outputManager->size = 0;

	deleteTree(root);
}
