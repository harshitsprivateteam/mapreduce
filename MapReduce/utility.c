//
//  utility.c
//  Samples
//
//  Created by Harshit Gupta on 3/14/14.
//  Copyright (c) 2014 Harshit Gupta. All rights reserved.
//

#include <stdio.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include "utility.h"

#define ERROR -1

long accessibleFileSize(const char *pathName)
{
	long size = ERROR;
	
	int fd = open(pathName, O_RDONLY);
	if (fd == ERROR) {
		perror("Calculating File Size");
	}
	else
	{
		struct stat buf;
		fstat(fd, &buf);
		size = buf.st_size;
		close(fd);
	}
	return size;
}

bool isFileAccessible(char *filename)
{
	bool flag = true;
	FILE *fileReader = fopen(filename, "r");
	if (fileReader == NULL)
		flag = false;
	else
		fclose(fileReader);
	return flag;
}
