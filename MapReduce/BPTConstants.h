//
//  BPTConstants.h
//  MapReduce
//
//  Created by Harshit Gupta on 9/13/14.
//  Copyright (c) 2014 Harshit Gupta. All rights reserved.
//

#ifndef MapReduce_BPTConstants_h
#define MapReduce_BPTConstants_h

#define treeOrder 10
#define middleKeyIndex (treeOrder+1)/2

#include <stdbool.h>
#include <stdint.h>

#endif
