//
//  ThreadPool.c
//  Samples
//
//  Created by Harshit Gupta on 3/15/14.
//  Copyright (c) 2014 Harshit Gupta. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include "ThreadPool.h"

pthread_mutex_t chechPoolEmptyLock = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t checkPoolFullLock = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t getThreadLock = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t addThreadLock = PTHREAD_MUTEX_INITIALIZER;

#define lock pthread_mutex_lock
#define unlock pthread_mutex_unlock
#define destroyMutex pthread_mutex_destroy

ThreadPool* initThreadPool(int maxSize)
{
	Stack stack;
	initStack(&stack, maxSize);
	
	int i;
	for (i = 0; i < maxSize; i++)
		stackPush(&stack, i);
	
	pthread_t threads[maxSize];
	
	ThreadPool *threadPool = (ThreadPool *) malloc(sizeof(ThreadPool));
	threadPool->threads = threads;
	threadPool->stack = stack;
	threadPool->used = stack.maxSize - (stack.top + 1);
	threadPool->max =  stack.maxSize;
	return threadPool;
}

void destroyThreadPool(ThreadPool *threadPool)
{
	threadPool->threads = NULL;
	destroyStack(&threadPool->stack);
	threadPool->used = 0;
	threadPool->max = 0;
	
	destroyMutex(&chechPoolEmptyLock);
	destroyMutex(&checkPoolFullLock);
	destroyMutex(&getThreadLock);
	destroyMutex(&addThreadLock);
}

bool isThreadPoolEmpty(ThreadPool *threadPool)
{
	bool flag = true;
	if (!lock(&chechPoolEmptyLock))
	{
		flag = isStackEmpty(&(threadPool->stack));
		unlock(&chechPoolEmptyLock);
	}
	return flag;
}

bool isThreadPoolFull(ThreadPool *threadPool)
{
	bool flag = true;
	if (!lock(&checkPoolFullLock))
	{
		flag = isStackFull(&(threadPool->stack));
		unlock(&checkPoolFullLock);
	}
	return flag;
}

void addThreadToPool(ThreadPool *threadPool, threadId element)
{
	if (!lock(&addThreadLock))
	{
		if (isThreadPoolFull(threadPool))
		{
			fprintf(stderr, "Can't add thread to pool: pool is full.\n");
			exit(1);
		}
		threadPool->used--;
		stackPush(&(threadPool->stack), (stackElementT) element);
		unlock(&addThreadLock);
	}
}

threadId getThreadFromPool(ThreadPool *threadPool)
{
	threadId tI = -1;
	if (!lock(&getThreadLock))
	{
		if (isThreadPoolEmpty(threadPool))
		{
			fprintf(stderr, "Can't get thread from pool: pool is empty.\n");
			exit(1);
		}
		threadPool->used++;
		tI = stackPop(&(threadPool->stack));
		unlock(&getThreadLock);
	}
	else
		perror("mutex lock");
	return tI;
}


