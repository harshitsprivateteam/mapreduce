//
//  List.h
//  MapReduce
//
//  Created by Harshit Gupta on 4/24/14.
//  Copyright (c) 2014 Harshit Gupta. All rights reserved.
//

#ifndef MapReduce_List_h
#define MapReduce_List_h

/*!
 *  Struct for managing all the values corresponding to a key
 */
typedef struct
{
	char **list;
	size_t used;
	size_t size;
} List;

/*!
 *  Initialize the list
 *
 *  @param value dummy pointer to value, to be allocated
 */
void initList(List *list);

/*!
 *  adds values to list
 *
 *  @param list pointer to list
 *  @param value char array to be added to list
 */
void addValueToList(List *list, char *value);

/*!
 *  Deallocates List
 */
void freeList(List *list);

#endif