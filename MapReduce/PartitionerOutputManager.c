//
//  PartitionerOutputManager.c
//  MapReduce
//
//  Created by Harshit Gupta on 4/24/14.
//  Copyright (c) 2014 Harshit Gupta. All rights reserved.
//

#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include "PartitionerOutputManager.h"
#include "murmur3.h"


void partitionData(MapOutputMgr *outputManager,
				   int numberOfPartitions,
				   char *outputFilenameFormat)
{
	char filename[MaxLengthFileName];
	int i = 0, fileDescriptors[numberOfPartitions];
	for (i = 0; i < numberOfPartitions; i++)
	{
		sprintf(filename, outputFilenameFormat, i);
		fileDescriptors[i] = creat(filename, S_IRUSR | S_IWUSR |S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH);
	}

	Pair *pair;
	List *list;
	int j = 0, currentIndex = 0;
	size_t offsetArray[numberOfPartitions];
	memset(offsetArray, 0, sizeof(offsetArray));
	size_t bytesWritten = 0;
	for (i = 0; i < outputManager->used; i++)
	{
		bytesWritten = 0;
		pair = outputManager->pairs[i];
		uint32_t hash[1];
		MurmurHash3_x86_32((unsigned char *) pair->key, (int)strlen(pair->key), 23, hash);
		currentIndex = hash[0] % numberOfPartitions;
		char ch[2];
		sprintf(ch, "%c", Splitter);

		bytesWritten = pwrite(fileDescriptors[currentIndex], pair->key, strlen(pair->key), offsetArray[currentIndex]);
		offsetArray[currentIndex] += bytesWritten;

		bytesWritten = pwrite(fileDescriptors[currentIndex], ch, strlen(ch), offsetArray[currentIndex]);
		offsetArray[currentIndex] += bytesWritten;

		list = &pair->list;
		for (j = 0; j < list->used; j++)
		{
			bytesWritten = pwrite(fileDescriptors[currentIndex], list->list[j], strlen(list->list[j]), offsetArray[currentIndex]);
			offsetArray[currentIndex] += bytesWritten;
			
			bytesWritten = pwrite(fileDescriptors[currentIndex], ch, strlen(ch), offsetArray[currentIndex]);
			offsetArray[currentIndex] += bytesWritten;
		}

		bytesWritten = pwrite(fileDescriptors[currentIndex], "\n", sizeof("\n"),offsetArray[currentIndex]);
		offsetArray[currentIndex] += bytesWritten;
	}
	for (i = 0; i < numberOfPartitions; i++)
		close(fileDescriptors[i]);
}
