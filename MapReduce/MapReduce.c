//
//  MapReduce.c
//  MapReduce
//
//  Created by Harshit Gupta on 4/22/14.
//  Copyright (c) 2014 Harshit Gupta. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <stdbool.h>
#include <fcntl.h>
#include <unistd.h>
#include "MapReduce.h"
#include "List.h"

#define Splitter '>'
#define Divider "  -->  "
#define NewLine "\n"
#define BracketStart "["
#define BracketStop "]"

#define MaxCountSize 10
#define MaxKeySize 1024
#define MaxValueSize 1024


void addPairsToOutputManager(MapOutputMgr *outputManager, char *key, List *list);
void listFromString(List *list, char *string);
void stringFromList(List *list, char *string);

void map(char *key,
		 char *value,
		 MapOutputMgr *outputManager,
		 unsigned long long startOffet,
		 unsigned long long endOffset)
{
	int  fileReader;
	
	fileReader = open(value, O_RDONLY);
	if (fileReader < 1)
	{
		if (errno == EACCES)
			printf("Map - Access Denied\n");
		else if(errno == ENOENT)
			printf("Map - File Not Found\n");
		else
			printf("Map - Error Unknown\n");
		exit(EXIT_FAILURE);
	}
	
	char ch[2], tempKey[MaxKeySize], tempValue[2];
	memset(ch, '\0', sizeof(ch));
	memset(tempKey, '\0', sizeof(tempKey));
	memset(tempValue, '\0', sizeof(tempValue));
	
	sprintf(tempValue, "%i", 1); //setting value to "1" denoting occurance of the word once in the document
	
	int keyCount = 0;
	size_t offset = startOffet, bytesRead = 0;

	while ((bytesRead = pread(fileReader, ch, 1, offset)) > 0)
	{
		offset += bytesRead;
		if ((ch[0] >= 65 && ch[0] <=90))
		{
			tempKey[keyCount++] = (ch[0]+32);
		}
		else if ((ch[0] >=97 && ch[0] <=122))
		{
			tempKey[keyCount++] = ch[0];
		}
		else
		{
			if (keyCount > 0) {
//				printf("key:%s--------\n", tempKey);
				updateWithPair(outputManager, tempKey, tempValue);
				memset(tempKey, '\0', sizeof(tempKey));
				keyCount = 0;
			}
		}
		if (offset == endOffset)
			break;
	}
}

void reduce(char *key, char **list, size_t totalValues, char *output)
{
	int count = 0, i = 0;
	
	for (i = 0; i < totalValues; i++)
		count+=atoi(list[i]);
	
	char countString[MaxCountSize];
	sprintf(countString, "%i", count);
	char tempOutput[MAXOUTPUTLENGTH];
	memset(tempOutput, '\0', MAXOUTPUTLENGTH);
	
	strncpy(tempOutput, BracketStart, strlen(BracketStart));
	strncat(tempOutput, key, strlen(key));
	strncat(tempOutput, Divider, strlen(Divider));
	strncat(tempOutput, countString, strlen(countString));
	strncat(tempOutput, BracketStop, strlen(BracketStop));
	strncat(tempOutput, NewLine, strlen(NewLine));
	
	strcpy(output, tempOutput);
}

