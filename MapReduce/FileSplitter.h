//
//  FileSplitter.h
//  Samples
//
//  Created by Harshit Gupta on 4/23/14.
//  Copyright (c) 2014 Harshit Gupta. All rights reserved.
//

#ifndef Samples_FileSplitter_h
#define Samples_FileSplitter_h

#include <stdbool.h>

/*!
 *  Split the files into number of parts asked, having almost equal size. One line is considerd as one record. While splitting, if one record
 *  comes in between two splits, the entire record will go to first split and the second split will start from the next record.
 *
 *  @param inputFileName    char array as input file name
 *  @param outputfileFormat char array as output file name format to write the split content.
 *  @param totalParts       number of total parts to be made.
 *
 *  @return true if succesfully done, false otherwise.
 */

bool splitFile(char *inputFileName, char *outputFileFormat, int totalParts);

#endif
