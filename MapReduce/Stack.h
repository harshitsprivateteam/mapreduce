//
//  Stack.h
//  Samples
//
//  Created by Harshit Gupta on 3/15/14.
//  Copyright (c) 2014 Harshit Gupta. All rights reserved.
//

#ifndef Samples_Stack_h
#define Samples_Stack_h

typedef int stackElementT;

/*!
 *  Data structure representing the variables used to manage of the stack.
 */
typedef struct
{
	stackElementT *contents;
	int top;
	int maxSize;
} Stack;

/*!
 *  Initializes the stack with the size specified and puts the refrence to pointer passed to it.
 *
 *  @param stack   pointer refrence to the stack
 *  @param maxSize size for the stack
 */
void initStack(Stack *stack, int maxSize);

/*!
 *  Deallocates the memory assigned to the stack pointer.
 *
 *  @param stack   pointer refrence to the stack
 */
void destroyStack(Stack *stack);

/*!
 *  Checks if the stack is empty
 *
 *  @param stack   pointer refrence to the stack
 *
 *  @return true if empty, false otherwise.
 */
int isStackEmpty(Stack *stack);

/*!
 *  Checks if the stack is full
 *
 *  @param stack   pointer refrence to the stack
 *
 *  @return true if full, false otherwise.
 */
int isStackFull(Stack *stack);

/*!
 *  Pushes the element specified to the stack.
 *
 *  @param stack   pointer refrence to the stack
 *  @param element the element to be pushed
 */
void stackPush(Stack *stack, stackElementT element);

/*!
 *  Pop the element from the top of the stack and returns it.
 *
 *  @param stack   pointer refrence to the stack
 *
 *  @return  element poped.
 */
stackElementT stackPop(Stack *stack);

#endif
