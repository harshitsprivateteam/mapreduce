//
//  NodesManager.h
//  MapReduce
//
//  Created by Harshit Gupta on 4/24/14.
//  Copyright (c) 2014 Harshit Gupta. All rights reserved.
//

#ifndef MapReduce_NodesManager_h
#define MapReduce_NodesManager_h

#include <unistd.h>
#include <stdbool.h>
#include "NetworkConstants.h"

/*!
 *  The enum structure represents all the possible values of a node of the cluster
 */
typedef enum
{
	NTClient,
	NTNameNode,
	NTDataNode
} NodeType;

/*!
 *  This structure consists of ip address, node type and port numbers for communication thread and ftp service
 */
typedef struct{
	char ipAddr[MaxLengthIPAddr];
	int port;
	int ftpPort;
	int heartbeatPort;
	NodeType nodeType;
} Node;

/*!
 *  This structure consists of 2d array of nodes, total size of nodelist and number of currently in nodelist.
 */
typedef struct {
	Node **nodeList;
	size_t used;
	size_t size;
} NodesManager;

/*!
 *  Initialize the Node manager passed with the size required.
 *
 *  @param filename name of the file, to read the configurations from.
 *
 *  @return   refernce pointer to the node manager
 */
NodesManager* initNodesManager(char *filename);

/*!
 *  Deallocates the node manager with all the nodes from nodelist.
 */
void freeNodesManager(NodesManager *nodesManager);

#endif
