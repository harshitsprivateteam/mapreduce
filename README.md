Emulating basic functionality of Hadoop(MapReduce + HDFS) via C language.

The setup includes two parts FileLoader and Hadoop.
1. FileLoader acts like a daemon running initially running on, Namenode and data nodes. Its main aim is to receive the code(build) to run,
   which is created compiled at the client node.
2. hadoop first of all runs on client node. It first reads a config file, based upon that the code is sent to namenode.
3. Once the download is complete, the name node runs hadoop code. It splits the input file to a "n" files for n mappers.
4. After splitiing is done at name node, it sends the hadoop code and the splitted file to corresponding mappers(datanodes).
5. The datanodes download those files parallely. Once its finished downloading, it runs the code.
6. The code then detects it own type. Runs the map function on the split file, combines the data and by partitioning, generate file "m" reducer input files and send them to corresponding reducers.
7. Reducers after receiving the files, runs reduce function on them. It then generates the result file and send it name node.
8. Name node after receiving the all teh result files. Forward those files to client node.
9. Client Node, after collecting all the results files, informs teh user.