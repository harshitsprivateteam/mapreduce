//
//  MapOutputManager.h
//  MapReduce
//
//  Created by Harshit Gupta on 4/22/14.
//  Copyright (c) 2014 Harshit Gupta. All rights reserved.
//

#ifndef MapReduce_MapOutputManager_h
#define MapReduce_MapOutputManager_h

#include "List.h"

#define MaxLengthFileName 25
#define Splitter '>'

/*!
 *  Pair
 *
 *  key		char array representing the key
 *  Value	collection of all the value for the key
 */
typedef struct
{
	char *key;
	List list;
} Pair;

/*!
 *  Map Output manager
 *
 */
typedef struct
{
	Pair **pairs;
	size_t used;
	size_t size;
} MapOutputMgr;

/*!
 *  Initialize the Output manager
 *
 *  @param outputManager dummy pointer to outputManager, to be allocated
 *
 *  @return success if succesfully created, failure otherwise
 */
void initMapOutputManager(MapOutputMgr *outputManager);

void initMapOutputManagerWithFile(MapOutputMgr *outputManager,
									   int numberOfFiles,
									   char* filenameFormat);

/*!
 *   Checks if a cpair already exists on the basis of key, if it does then the value of the pair, passed is added to the list
 *   Otherwise, a new cpair is created and add to manager.
 *   Reallocates memory when needed.
 *
 *  @param outputManager pointer to output manager
 *  @param pair          pointer to pair variable having key and value
 */
void updateWithPair(MapOutputMgr *outputManager, char *key, char *value);

/*!
 *  Deallocates outputManager
 *
 *  @param outputManager pointer to outputmanager to be deallocated
 */
void freeMapOutputManager(MapOutputMgr *outputManager);

#endif