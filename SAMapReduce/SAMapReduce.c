//
//  SAMapReduce.c
//  MapReduce
//
//  Created by Harshit Gupta on 4/22/14.
//  Copyright (c) 2014 Harshit Gupta. All rights reserved.
//

#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>              // for exit()
#include <string.h>
#include <unistd.h>
#include "murmur3.h"
#include "BPlusTree.h"
#include <sys/time.h>

#include "MapOutputManager.h"
#include "MapReduce.h"
#include "utility.h"

//#define InputFilename "shapes.txt"
//#define InputFilename "friendsList.txt"
#define InputFilename "textfile.txt"
#define ReduceOutputFileFormat "result.txt"

int main(int argc, const char * argv[])
{
	char *key = "key";
	char value[MaxLengthFileName];
	strcpy(value, InputFilename);
	
	struct timeval  tv1, tv2;
	gettimeofday(&tv1, NULL);
	
	MapOutputMgr MapOutputMgr;
	initMapOutputManager(&MapOutputMgr);
	map(key, value, &MapOutputMgr);
	
	unlink(ReduceOutputFileFormat);
	int outputFileDescriptor = creat(ReduceOutputFileFormat, S_IRUSR | S_IRGRP | S_IROTH);
	char *output;
	size_t bytesWritten = 0;
	int currentOffset = 0;
	
	int i = 0;
	for (i = 0; i < MapOutputMgr.used; i++)
	{
		output = calloc(MAXOUTPUTLENGTH, sizeof(char));
		
		reduce(MapOutputMgr.pairs[i]->key,
			   MapOutputMgr.pairs[i]->list.list,
			   MapOutputMgr.pairs[i]->list.used,
			   output);
		bytesWritten =  pwrite(outputFileDescriptor, output, strlen(output), currentOffset);
		currentOffset += bytesWritten;
//		printf("%s", output);
		
		if (output != NULL)
			free(output);
	}
	printf("\nTotal Words: %zu\n", MapOutputMgr.used);
	close(outputFileDescriptor);
	
	gettimeofday(&tv2, NULL);
	printf ("Total time = %f seconds\n",
			(double) (tv2.tv_usec - tv1.tv_usec) / 1000000 +
			(double) (tv2.tv_sec - tv1.tv_sec));
	
	freeMapOutputManager(&MapOutputMgr);
	return 0;
}
