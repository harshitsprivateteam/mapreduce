//
//  MapReduce.h
//  MapReduce
//
//  Created by Harshit Gupta on 4/22/14.
//  Copyright (c) 2014 Harshit Gupta. All rights reserved.
//

#ifndef MapReduce_MapReduce_h
#define MapReduce_MapReduce_h

#include "MapOutputManager.h"
#define MAXOUTPUTLENGTH 1024

/*!
 *  Map function to be implemented by the client
 *
 *  @param key           char array as key for the document
 *  @param value         char array as name of the document
 *  @param outputManager pointer to outputmanager to which all the pairs, will be added
 */
void map(char *key, char *value, MapOutputMgr *outputManager);

/*!
 *  Reduce function to be implemented by the client
 *
 *  @param key          char array as key for the list
 *  @param list         collection(2D pointer) of all the values
 *  @param totalValues  total number of rows in list(2D pointer).
 */
void reduce(char *key, char **list, size_t totalValues, char *output);

#endif