//
//  BPlusTree.c
//  MapReduce
//
//  Created by Harshit Gupta on 9/13/14.
//  Copyright (c) 2014 Harshit Gupta. All rights reserved.
//

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "BPlusTree.h"

void* insertKeyInTree(BPTNode *node, uint32_t *key, int *keyIndex, void *pointer);
bool insertKeyInNode(BPTNode *node, uint32_t key, int keyIndex,void *pointer);
int indexToProceedForKey(BPTNode *node, uint32_t key);

BPTNode* initBPTNode()
{
	BPTNode *node = (BPTNode *) malloc(sizeof(BPTNode));
	
	node->noOfKeys = 0;
	node->isLeaf = true;

	memset(node->keys, 0, sizeof(node->keys));
	memset(node->keyIndeces, -1, sizeof(node->keyIndeces));
	memset(node->pointers, 0, sizeof(node->pointers));
	return node;
}

BPTNode* insertKey(BPTNode *root, uint32_t key, int keyIndex)
{
	if (root == NULL)
		root = initBPTNode();

	BPTNode *newNode = (BPTNode *)insertKeyInTree(root, &key, &keyIndex, NULL);
	if (newNode != NULL)
	{
		BPTNode *newRoot = initBPTNode();
		newRoot->isLeaf = false;
		
		newRoot->keys[newRoot->noOfKeys] = key;
		newRoot->keyIndeces[newRoot->noOfKeys] = keyIndex;
		newRoot->pointers[newRoot->noOfKeys] = root;
		newRoot->noOfKeys++;
		
		newRoot->pointers[newRoot->noOfKeys] = newNode;
		
		root = newRoot;
	}
	return root;
}

void* insertKeyInTree(BPTNode *node, uint32_t *key, int *keyIndex, void *pointer)
{
	bool toInsertNow = true;
	
	if (!node->isLeaf)
	{
		pointer = insertKeyInTree((BPTNode *)node->pointers[indexToProceedForKey(node, *key)],
								  key,
								  keyIndex,
								  pointer);
		
		if (pointer == NULL)
			toInsertNow = false;
	}
	
	if (toInsertNow)
	{
		if (insertKeyInNode(node, *key, *keyIndex, pointer))
			pointer = NULL;
		else
		{
			int j = node->isLeaf ? 0 : 1;
			
			BPTNode *previousNode, *newNode;
			previousNode = node;
			
			uint32_t tempKeyArray[treeOrder + 1];
			int tempKeyIndecesArray[treeOrder + 1];
			
			memcpy (tempKeyArray, previousNode->keys, sizeof(previousNode->keys)) ;
			memcpy (tempKeyIndecesArray, previousNode->keyIndeces, sizeof(previousNode->keyIndeces)) ;

			memset(previousNode->keys, 0, sizeof(previousNode->keys)); // resetting automatically-allocated key array
			memset(previousNode->keyIndeces, -1, sizeof(previousNode->keyIndeces)); // resetting automatically-allocated keyIndeces array
			
			void *temp = node->isLeaf ? previousNode->pointers[treeOrder]:NULL;
			
			void* tempPointers[treeOrder+2];
			memcpy (tempPointers, previousNode->pointers, sizeof (previousNode->pointers)) ;
			memset(previousNode->pointers, 0, sizeof(previousNode->pointers)); // resetting automatically-allocated pointers array
			
			int i=0;
			
			for (i = treeOrder-1; tempKeyArray[i]>*key && i>=0; i--)
			{
				tempKeyArray[i+1] = tempKeyArray[i];
				tempKeyIndecesArray[i+1] = tempKeyIndecesArray[i];
				tempPointers[i+1+j] = tempPointers[i+j];
			}
			
			tempKeyArray[i+1] = *key;
			tempKeyIndecesArray[i+1] = *keyIndex;
			tempPointers[i+1+j] = pointer;
			
			previousNode->noOfKeys = middleKeyIndex; // emptying the previous node
			
			for (i = 0; i<middleKeyIndex; i++) // filling the previous node with sorted value
			{
				previousNode->keys[i] = tempKeyArray[i];
				previousNode->keyIndeces[i] = tempKeyIndecesArray[i];
				previousNode->pointers[i] = tempPointers[i];
			}
			
			newNode = initBPTNode();
			newNode->isLeaf = previousNode->isLeaf;
			newNode->pointers[treeOrder] = temp;
			
			int startIndex = middleKeyIndex;
			
			if (!previousNode->isLeaf)
			{
				previousNode->pointers[i] = tempPointers[i]; // as its node copying *45 to it
				newNode->pointers[treeOrder/2] = tempPointers[treeOrder+1];
				startIndex++;
			}
			else
				previousNode->pointers[treeOrder] = newNode;
			
			for (i = startIndex; i < treeOrder+1; i++) // filling the new node
			{
				newNode->keys[newNode->noOfKeys] = tempKeyArray[i];
				newNode->keyIndeces[newNode->noOfKeys] = tempKeyIndecesArray[i];
				newNode->pointers[newNode->noOfKeys] = tempPointers[i];
				newNode->noOfKeys++;
			}
			
			*key = tempKeyArray[middleKeyIndex];
			*keyIndex = tempKeyIndecesArray[middleKeyIndex];
			pointer = newNode;
		}
	}
	
	return pointer;
}

bool insertKeyInNode(BPTNode *node, uint32_t key, int keyIndex, void *pointer)
{
	if (node->noOfKeys == treeOrder)
		return false; // node is full. Can't insert.
	
	int i=0;
	int j = node->isLeaf ? 0:1;
	for (i = (node->noOfKeys-1); node->keys[i]>key; i--)
	{
		node->keys[i+1] = node->keys[i];
		node->keyIndeces[i+1] = node->keyIndeces[i];
		node->pointers[i+1+j] = node->pointers[i+j];
	}
	
	node->keys[i+1] = key;
	node->keyIndeces[i+1] = keyIndex;
	node->pointers[i+1+j] = pointer;
	
	node->noOfKeys++;
	return true;
}

int indexToProceedForKey(BPTNode *node, uint32_t key)
{
	int i;
	for(i = 0; i<node->noOfKeys && node->keys[i]<=key ; i++);
	return i;
}

int searchKeyInTree(BPTNode *node, uint32_t key)
{
	int index = -1;

	if (node != NULL)
	{
		if (!node->isLeaf)
			index = searchKeyInTree((BPTNode *)node->pointers[indexToProceedForKey(node, key)],
									key);
		else
		{
			int i;
			for (i = 0; ((i < node->noOfKeys) && (key >= node->keys[i])); i++)
			{
				if (node->keys[i] == key)
				{
					index = node->keyIndeces[i];
					break;
				}
			}
		}
	}

	return index;
}

void deleteTree(BPTNode *node)
{
	if (!node->isLeaf)
	{
		int i;
		for (i = 0; i<=node->noOfKeys; i++)
		{
			deleteTree((BPTNode *)node->pointers[i]);
		}
	}
	free(node);
}
