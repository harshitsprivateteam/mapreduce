//
//  LoadMonitorClient.c
//  Samples
//
//  Created by Harshit Gupta on 6/25/14.
//  Copyright (c) 2014 Harshit Gupta. All rights reserved.
//

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

typedef struct
{
	long double cpuUtiz;
	long double freeMem;
	unsigned numOfCores;
} SystemInfo;

int main(int argc, const char * argv[])
{
    long double a[4], b[4];
    FILE *fp;
	char nCoreChar[4];
	memset(nCoreChar, '\0', 4);

	SystemInfo loadInfo;
	
	fp = fopen("/proc/stat","r");
	fscanf(fp,"%*s %Lf %Lf %Lf %Lf",&a[0],&a[1],&a[2],&a[3]);
	fclose(fp);
	sleep(1);
	
	fp = fopen("/proc/stat","r");
	fscanf(fp,"%*s %Lf %Lf %Lf %Lf",&b[0],&b[1],&b[2],&b[3]);
	fclose(fp);
	
	loadInfo.cpuUtiz = ((b[0]+b[1]+b[2]) - (a[0]+a[1]+a[2])) / ((b[0]+b[1]+b[2]+b[3]) - (a[0]+a[1]+a[2]+a[3]));
	
	fp = fopen("/proc/meminfo","r");
	fscanf(fp,"%*s %Lf kB\n %*s %Lf",&a[0], &loadInfo.freeMem);
	fclose(fp);
	
	fp = popen("grep -c \"cpu cores\" /proc/cpuinfo","r");
	fgets(nCoreChar, sizeof(nCoreChar)-1, fp);
	loadInfo.numOfCores = atoi(nCoreChar);
	fclose(fp);
	
	printf("CPU utilization : %Lf\n", loadInfo.cpuUtiz);
	printf("Free Memory : %Lf\n",loadInfo.freeMem);
	printf("# cores : %u\n",loadInfo.numOfCores);

    return(0);
}

