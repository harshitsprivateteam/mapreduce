//
//  List.c
//  MapReduce
//
//  Created by Harshit Gupta on 4/24/14.
//  Copyright (c) 2014 Harshit Gupta. All rights reserved.
//

#include <stdio.h>
#include "List.h"
#include <stdlib.h>
#include <string.h>

#pragma List functions

void initList(List *list)
{
	int initialSize = 1;
	list->list = (char **)calloc(initialSize, sizeof(char *));
	list->used = 0;
	list->size = initialSize;
}

void addValueToList(List *list, char *value)
{
	if (list->used == list->size)
	{
		list->size *= 2;
		list->list = (char **)realloc(list->list, list->size * sizeof(char *));
	}
	
	list->list[list->used] = (char *)calloc( strlen(value) + 1, sizeof(char));
	
	stpncpy(list->list[list->used], value, strlen(value));
	
	list->used++;
}

void freeList(List *list)
{
	int i = 0 ;
	
	for (i = 0; i < list->used ; i++)
		free(list->list[i]);
	
	free(list->list);
	list->list = NULL;
	list->used = list->size = 0;
}

