//
//  FileLoader.c
//  Samples
//
//  Created by Harshit Gupta on 4/26/14.
//  Copyright (c) 2014 Harshit Gupta. All rights reserved.
//

#include <stdio.h>			//for printf(), ...
#include <sys/types.h>		//for data types
#include <stdlib.h>			//for atoi() and exit()
#include <string.h>			//for memset()
#include <sys/socket.h>		//for socket(), connect(), ...
#include <unistd.h>			//for close()
#include <netinet/in.h>		//for internet address family
#include <arpa/inet.h>		//for sockaddr_in, inet_addr()
#include <errno.h>			//for system error numbers
#include <pthread.h>		//for pthread functions
#include <fcntl.h>			//for open()
#include <sys/stat.h>		//for chmod()

#include "ThreadPool.h"
#include "NodesManager.h"
#include "NetworkConstants.h"

#define MAXParallerDownloads 100

#ifdef __unix__
	#define ConfigFile "config.txt"
#elif defined __APPLE__
	#define ConfigFile "config_m.txt"
#endif

#define ProjectName "FDMapReduce"

/*!
 *  This data structure holds the data that needs to be transfer to the function pointer.
 */
typedef struct
{
	SocketDescriptor remoteSocketDes;
	threadId threadNumber;
	Node *node;
	char filename[MaxLengthFileName];
} ThreadData;

/*!
 *  The variable helps the program keep its index in track in order to interact with other nodes.
 */
int selfIndex;

/*!
 *  This getter function returns to refrence to the thread pool. A thread manages everything relaetd to thread like how many are currently used, how many available. It works on the basis of reusability.
 *
 *  @return pointer refrence of type thread pool
 */
ThreadPool* localThreadPool();

/*!
 *  This getter function returns to refrence to the local node manager, which is mostly used to find a refrence to other nodes.
 *
 *  @return pointer refrence of type Node Manager
 */
NodesManager* localNodeManager();

/*!
 *  This getter method return the caller, a refrence to the information associated with the local node.
 *
 *  @return pointer refrence of type Node
 */
Node* localNode();

/*!
 *  This returns reference to node which has the same ip address and listens to the same port number as mentioned.
 *
 *  @param ipAddr character array having the ipAddr to be matched.
 *  @param port   int port number to be matched.
 *
 *  @return pointer refrence of type Node
 */
Node* nodeWithIPNPort(char *ipAddr, int port);

/*!
 *  This pointer functions is further called by downloadFiles method to download teh files in parallel.
 *
 *  @param threadData a data struture containing the information that might be needed by the thread.
 *
 *  @return NULL
 */
void *downloadFile(void *threadData);

/*!
 *  This function checks if the file is executable, from the name of of the file.
 *
 *  @param filename filename to check for
 *
 *  @return bool value, true if executable, false otherwise.
 */
bool isFileExecutable(char *filename);

/*!
 *  Whenever some error occurs, this methods is called. It displays the error message and then exits the whole programs.
 *
 *  @param message the error message to be displayed.
 */
void exitUponError(char *message);

int main(int argc, char *argv[])
{
	argc > 1 ? selfIndex = atoi(argv[1]) : exitUponError("Insufficient Argument");
	
	if (selfIndex < 0 || selfIndex >= localNodeManager()->used)
		exitUponError("Invalid argument.");
	
	SocketDescriptor localSocketDes, remoteSocketDes;
	Socket localSocket, remoteSocket;
	socklen_t socketLength;
	
	if ((localSocketDes = socket(AF_INET, SOCK_STREAM, PF_UNSPEC)) == ERROR)
		exitUponError("socket");
	
	localSocket.sin_family = AF_INET;
	localSocket.sin_port = htons(localNode()->ftpPort);
	localSocket.sin_addr.s_addr = INADDR_ANY;
	bzero(&localSocket.sin_zero, 8);
	
	if (bind(localSocketDes, (struct sockaddr *) &localSocket, sizeof(localSocket)) == ERROR)
		exitUponError("bind");
	
	if (listen(localSocketDes, 1) == ERROR)
		exitUponError("listen");
	
	printf("Waiting for client's request....\n");
	socketLength = sizeof(Socket);
	
	remoteSocketDes = accept(localSocketDes,
							 (struct sockaddr *) &remoteSocket,
							 &socketLength);
	
	if (remoteSocketDes == ERROR)
		exitUponError("accept");
	
	printf ("New Client connected from port no %d and IP %s\n",
			ntohs(remoteSocket.sin_port),
			inet_ntoa(remoteSocket.sin_addr));
	
	MPMessage message = {0};
	size_t dataLength = recv (remoteSocketDes,
							  &message,
							  sizeof(MPMessage), 0);
	if (dataLength == ERROR || dataLength == 0)
		perror("recv");
	else
	{
		printf("here\n");
		ThreadPool *threadPool = localThreadPool();
		pthread_t thread;
		pthread_t usedThreadArray[message.numberOfFiles];
		int actualThreadsCount = 0;
		ThreadData *threadData = NULL;
		int i = 0;
		for (i  = 0; i < message.numberOfFiles; i++)
		{
			if (!isThreadPoolEmpty(threadPool))
			{
				threadId id = getThreadFromPool(threadPool);
				if (id != -1)
				{
					thread = threadPool->threads[id];
					
					threadData = (ThreadData *) malloc(sizeof(ThreadData));
					threadData->threadNumber = id;
					threadData->node = nodeWithIPNPort(inet_ntoa(remoteSocket.sin_addr), message.port);
					strcpy(threadData->filename, message.names[i]);
					pthread_create(&thread, NULL, downloadFile, threadData);
					
					usedThreadArray[actualThreadsCount] = thread;
					actualThreadsCount++;
				}
				else
					printf("Exception Occured. Closing connection.\n");
			}
			else
				printf("Reached Max Limit simultaneous downloads\n");
		}
		for (i  = 0; i < actualThreadsCount; i++)
			pthread_join(usedThreadArray[i], NULL);
		system("clear");
		system("clear");
		char cmd[MaxLengthFileName * 2];
		if (localNode()->nodeType == NTNameNode)
			sprintf(cmd, "./%s %i %s", ProjectName, selfIndex, message.filename);
		else
			sprintf(cmd, "./%s %i %s %llu %llu",
					ProjectName,
					selfIndex,
					message.filename,
					message.offset.startOffset,
					message.offset.endOffset);
		system(cmd);
	}
	close(remoteSocketDes);
	close(localSocketDes);
	return 0;
}

void *downloadFile(void *data)
{
	ThreadData threadData = *(ThreadData *)data;
	
	SocketDescriptor localSocketDes;
	Socket serverSocket;
	
	if ((localSocketDes = socket(AF_INET, SOCK_STREAM, PF_UNSPEC)) == ERROR)
		exitUponError("socket");
	
	serverSocket.sin_family = AF_INET;
	serverSocket.sin_port = htons(threadData.node->port);
	serverSocket.sin_addr.s_addr = inet_addr(threadData.node->ipAddr);
	bzero(&serverSocket.sin_zero, 8);
	
	if ((connect(localSocketDes, (struct sockaddr *) &serverSocket, SocketLength)) == ERROR)
		exitUponError("connect");
	
	MPMessage message = {0};
	strcpy(message.filename, threadData.filename);
	message.signalType = STFileNeeded;
	send(localSocketDes, &message, sizeof(MPMessage), 0);
	
	int fileDescriptor;
	long msgLength = 0;
	char content[MaxMessageSize];
	
	fileDescriptor = open(threadData.filename, O_CREAT|O_WRONLY, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
	if (fileDescriptor > 0)
		while ((msgLength = recv(localSocketDes, content, MaxMessageSize, 0)) > 0)
			write(fileDescriptor, content, msgLength);
	
	if (isFileExecutable(threadData.filename))
		chmod(threadData.filename, S_IRWXU | S_IRWXO | S_IRWXG);
	else
		chmod(threadData.filename, S_IRWXU | S_IRWXO | S_IRWXG);
	
	printf("File named %s downloaded\n", threadData.filename);
	close(fileDescriptor);
	free(data);
	addThreadToPool(localThreadPool(), threadData.threadNumber);
	return NULL;
}

void exitUponError(char *message)
{
	perror(message);
	exit(0);
}

ThreadPool* localThreadPool()
{
	static ThreadPool *threadPool;
	if (threadPool == NULL)
		threadPool = initThreadPool(MAXParallerDownloads);
	return threadPool;
}

bool isFileExecutable(char *filename)
{
	bool flag = true;
	int i = 0;
	for (i = 0; i < strlen(filename); i++)
		if (filename[i] == '.') {
			flag = false;
			break;
		}
	
	return flag;
}

Node* nodeWithIPNPort(char *ipAddr, int port)
{
	Node *node = NULL;
	NodesManager *nodesManager = localNodeManager();
	int i = 0;
	for (i = 0; i < nodesManager->used; i++)
		if (!strcmp(nodesManager->nodeList[i]->ipAddr, ipAddr) && nodesManager->nodeList[i]->port == port)
		{
			node = nodesManager->nodeList[i];
			break;
		}
	
	return node;
}

NodesManager* localNodeManager()
{
	static NodesManager *nodesManager;
	if (nodesManager == NULL)
		nodesManager = initNodesManager(ConfigFile);
	return nodesManager;
}

Node* localNode()
{
	return localNodeManager()->nodeList[selfIndex];
}
