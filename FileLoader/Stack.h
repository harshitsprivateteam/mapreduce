//
//  Stack.h
//  Samples
//
//  Created by Harshit Gupta on 3/15/14.
//  Copyright (c) 2014 Harshit Gupta. All rights reserved.
//

#ifndef Samples_Stack_h
#define Samples_Stack_h

typedef int stackElementT;

typedef struct
{
	stackElementT *contents;
	int top;
	int maxSize;
} Stack;

void initStack(Stack *stack, int maxSize);
void destroyStack(Stack *stack);

int isStackEmpty(Stack *stack);
int isStackFull(Stack *stack);

void stackPush(Stack *stack, stackElementT element);
stackElementT stackPop(Stack *stack);

#endif
