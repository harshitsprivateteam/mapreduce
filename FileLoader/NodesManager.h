//
//  NodesManager.h
//  MapReduce
//
//  Created by Harshit Gupta on 4/24/14.
//  Copyright (c) 2014 Harshit Gupta. All rights reserved.
//

#ifndef MapReduce_NodesManager_h
#define MapReduce_NodesManager_h

#include <unistd.h>
#include <stdbool.h>
#include "NetworkConstants.h"

typedef enum
{
	NTClient,
	NTNameNode,
	NTDataNode
} NodeType;

/*----------------------------------------------------------------
 This structure encapsulates all the varibales related to one node
 ----------------------------------------------------------------*/
typedef struct
{
	NodeType nodeType;
} LocalData;

/*----------------------------------------------------------------
 This structure consists of ip address, port number for receiving
 and sending broadcast messages and port number for tcp interaction
 with the neighbors.
 ----------------------------------------------------------------*/
typedef struct{
	char ipAddr[MaxLengthIPAddr];
	int port;
	int ftpPort;
	int heartbeatPort;
	NodeType nodeType;
} Node;

/*----------------------------------------------------------------
 This structure consists of 2d array of nodes, total size of nodelist
 and number of currently in nodelist.
 ----------------------------------------------------------------*/
typedef struct {
	Node **nodeList;
	size_t used;
	size_t size;
} NodesManager;

/*----------------------------------------------------------------
 Initialize the Node manager passed with the size required.
 ----------------------------------------------------------------*/
NodesManager* initNodesManager(char *filename);

/*----------------------------------------------------------------
 Deallocates the node manager with all the nodes from nodelist.
 ----------------------------------------------------------------*/
void freeNodesManager(NodesManager *nodesManager);

#endif
