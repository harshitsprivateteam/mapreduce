/********************************************************************
 
 ***** NodesManager.c *****
 
 Computer Science 6823 Distributed Systems
 
 Spring 2014
 
 Instructor:  Hai Jiang
 
 Assignment # 1
 
 Due Date:    Tuesday, Mar. 04, 2014
 
 Description:	This program provides functions for initialization and deallocation of a node manager, along with the capability of inserting nodes.
 
 *******************************************************************/

#include "NodesManager.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define Delineator ':'

/*----------------------------------------------------------------
 Insert the node to the nodelist of node manager.
 Reallocate memory when needed.
 Takes a str argument to calculate the values for the new node.
 ----------------------------------------------------------------*/
void insertNodesManager(NodesManager *nodesManager, char str[]);

/*----------------------------------------------------------------
 Extract the filename passed and insert nodes to list.
 ----------------------------------------------------------------*/
int extractConfigFile(NodesManager *nodesManager, char *fileName);


NodesManager* initNodesManager(char *filename)
{
	int initialSize = 1;
	NodesManager *nodesManager = (NodesManager *) malloc(sizeof(NodesManager));
	nodesManager->nodeList = (Node **)calloc(initialSize, sizeof(Node *));
	nodesManager->used = 0;
	nodesManager->size = initialSize;
	extractConfigFile(nodesManager, filename);
	return nodesManager;
}

int extractConfigFile(NodesManager *nodesManager, char *fileName)
{
	int flag = 0;
	FILE *fp = fopen(fileName, "r");
	
	if (fp != NULL)
	{
		flag = 1;
		char str[MaxLengthConfigLine];
		while(fgets (str, MaxLengthConfigLine, fp) != NULL )
			insertNodesManager(nodesManager, str);
		fclose(fp);
	}
	else
		exit(0);
	
	return flag;
}

void insertNodesManager(NodesManager *nodesManager, char str[])
{
	if (nodesManager->used == nodesManager->size)
	{
		nodesManager->size *= 2;
		nodesManager->nodeList = (Node **)realloc(nodesManager->nodeList, nodesManager->size * sizeof(Node *));
	}
	
	nodesManager->nodeList[nodesManager->used] = (Node *)calloc(1, sizeof(Node));
	
	int i = 0, delineatorIndex = 0;
	for(i = 0; i<strlen(str); i++)
	{
		if(str[i] == Delineator)
		{
			if(delineatorIndex == 0)
				nodesManager->nodeList[nodesManager->used]->ipAddr[i] = '\0';
			delineatorIndex++;
			continue;
		}
		
		if (str[i] == '\n')
			break;
		
		switch(delineatorIndex)
		{
			case 0:
				nodesManager->nodeList[nodesManager->used]->ipAddr[i] = str[i];
				break;
			case 1:
				nodesManager->nodeList[nodesManager->used]->port = nodesManager->nodeList[nodesManager->used]->port * 10 + (str[i] - '0');
				break;
			case 2:
				nodesManager->nodeList[nodesManager->used]->ftpPort = nodesManager->nodeList[nodesManager->used]->ftpPort * 10 + (str[i] - '0');
				break;
			case 3:
				nodesManager->nodeList[nodesManager->used]->heartbeatPort = nodesManager->nodeList[nodesManager->used]->heartbeatPort * 10 + (str[i] - '0');
				break;
			case 4:
				if (str[i] == 'N')
					nodesManager->nodeList[nodesManager->used]->nodeType = NTNameNode;
				else if (str[i] == 'D')
					nodesManager->nodeList[nodesManager->used]->nodeType = NTDataNode;
				else
					nodesManager->nodeList[nodesManager->used]->nodeType = NTClient;
			default:
				break;
		}
	}
	nodesManager->used++;
}

void freeNodesManager(NodesManager *nodesManager)
{
	int i = 0 ;
	
	for (i = 0; i < nodesManager->used ; i++) {
		free(nodesManager->nodeList[i]);
	}
	free(nodesManager->nodeList);
	nodesManager->nodeList = NULL;
	nodesManager->used = nodesManager->size = 0;
}