//
//  ThreadPool.h
//  Samples
//
//  Created by Harshit Gupta on 3/15/14.
//  Copyright (c) 2014 Harshit Gupta. All rights reserved.
//

#ifndef Samples_ThreadPool_h
#define Samples_ThreadPool_h

typedef int threadId;

#include "Stack.h"
#include <pthread.h>
#include <stdbool.h>

typedef struct
{
	pthread_t *threads;
	Stack stack;
	int used;
	int max;
} ThreadPool;

ThreadPool* initThreadPool(int maxSize);
void destroyThreadPool(ThreadPool *threadPool);

bool isThreadPoolEmpty(ThreadPool *threadPool);
bool isThreadPoolFull(ThreadPool *threadPool);

void addThreadToPool(ThreadPool *threadPool, threadId element);
threadId getThreadFromPool(ThreadPool *threadPool);

#endif
